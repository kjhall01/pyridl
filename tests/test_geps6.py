import src as iri 
import datetime as dt 

geps6 = iri.dl.models.subx.eccc.geps6.hindcast.pr
geps6 = geps6.rangeedges('X', 180-25, 180-5).rangeedges('Y', -5,25).average('M').sum('L')

tp = iri.TrainingPeriod(iri.Day(1992, 'Jul', 1), iri.Day(2010, 'Sep', 30)) 

lead_time = 7
target_length = 14 

geps6 = geps6.kairoz(lead_time, target_length, tp, overlap_targets=False, target_within_season=False, verbose=True)
start = dt.datetime.now()
ds = geps6.stream_download(verbose=True)
end = dt.datetime.now() 
print(end - start)
print(ds)