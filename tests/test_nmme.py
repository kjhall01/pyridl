import src as iri 
import datetime as dt


can = iri.dl.models.nmme.cansipsv2.hindcast.monthly.prec
can = can.rangeedges('X', 180-25, 180-5).rangeedges('Y', 5,25).average('M').sum('L')

tp = iri.TrainingPeriod(iri.Day(1991, 'Apr', 1), iri.Day(2020, 'Nov', 30)) 

lead_time = 4
target_length = 3

can = can.kairoz(lead_time, target_length, tp, units='months', verbose=True)
start  = dt.datetime.now() 
ds = can.stream_download(verbose=True)
end = dt.datetime.now() 


print(end-start)
print(ds)