import xarray as xr 
from pathlib import Path 
import re, sys , warnings, json
from dask.diagnostics import ProgressBar
import dask as d
from .day import Day
from .utilities import * 

class DataSource: 
    def __init__(self, path, parent=False, find=True): 

        self.path = path
        self.parent = parent
        self.isroot = True if parent is False else False
        self.banned = ['path', 'parent', 'isroot', 'banned', 'coords', 'units','aggregations', 'sliced_s', 'sliced_l' , 'restrictions', 'may_connect', 'iri_url', 'has_restricted']
        self.iri_url = self.url()
        self.sliced_s, self.sliced_l = None, None 
        self.restrictions, self.aggregations = {}, {}
        self.may_connect = False
        self.coords = None

        if find:
            self._find_subsource()

    def to_json(self): 
        ret = {}   
        for child in vars(self).keys():
            if child != 'parent':
                if type(getattr(self, child)) in [str, list, bool, dict]: 
                    ret[child] = getattr(self, child )
                elif type(getattr(self, child)) is  DataSource : 
                    ret[child] = getattr(self, child).to_json()
                else: 
                    ret[child] = str(getattr(self, child))
        return ret

    def save(self, fname):
        with open(fname, 'w') as f: 
            json.dump(self.to_json(), f, indent=4) 


    def from_dict(self, dict1): 
        for key in dict1.keys():
            if type(dict1[key]) != dict or key in ['coords', 'units', 'restrictions', 'aggregations']: 
                setattr(self, key, dict1[key])
            else: 
                new = DataSource('.', find=False, parent=self).from_dict(dict1[key])
                setattr(self, key.lower(), new )
        return self


    def avail(self, depth=1, level=1):
        print('  ' * (level-1) + str(self.path).split('/')[-1])
        for child in vars(self).keys(): 
            if child not in self.banned: 
                if  type(getattr(self, child)) is not DataSource: 
                    print('  ' * level + child)
                else:
                    if  level < depth: 
                        getattr(self, child).avail(depth=depth, level=level+1)
                    else: 
                        print('  ' * level + child)

    def reset(self):
        self.iri_url = self.url()

    def search(self, searchterm, depth=3, level=1): 
        for child in vars(self).keys(): 
            if searchterm in child and type(getattr(self, child)) is DataSource:
                print(str(getattr(self, child).url()))
            if child not in self.banned: 
                if  level < depth and type(getattr(self, child)) is DataSource: 
                    getattr(self, child).search( searchterm, depth=depth, level=level+1)


    def _format_url(self, s_chunks=None):
        #add restrictions to URL 
        self.iri_url = self.url()
        if s_chunks is None: # not chunked right now
            if 'S' in self.restrictions.keys(): 
                self.iri_url += '{}/{}/{}/RANGEEDGES/'.format('S', self.restrictions['S'][0], self.restrictions['S'][1]) 
            if 'L' in self.restrictions.keys(): 
                self.iri_url += '{}/{}/{}/RANGEEDGES/'.format('L', self.restrictions['L'][0], self.restrictions['L'][1]) 
        else: 
            self.iri_url += '{}/{}/{}/RANGEEDGES/'.format('S', s_chunks[0], s_chunks[-1]) 
            if self.sliced_l is not None: 
                self.iri_url += '{}/{}/{}/RANGEEDGES/'.format('L', self.sliced_l[0], self.sliced_l[-1])
            else: 
                assert False, 'You need to call Kairoz first'

        keys = sorted([ key for key in self.restrictions.keys()]) #order will be L M P  X Y Z
        for key in range(len(keys)-1, -1, -1):  # loop backwards Z Y X  P M L  and do S first 
            if key not in [ 'S', 'L' ] :
                self.iri_url += '{}/{}/{}/RANGEEDGES/'.format(keys[key], self.restrictions[keys[key]][0], self.restrictions[keys[key]][1]) 
    
        #add aggregations to URL
        for key in self.aggregations.keys(): 
            string1 = '['
            for dim in self.aggregations[key]: 
                string1 += '{} '.format(dim)
            string1 += ']/{}/'.format(key)
            self.iri_url += string1

        
    def download(self, overwrite_cache=True, verbose=False):
        if self.may_connect:
            if len(self.restrictions.keys()) < 3 or 'S' not in self.restrictions.keys(): 
                warnings.warn('Downloading Data without restricting Ranges can be very slow or fail! Kairoz does not restrict S or L for this - use chunk_download ')
            
            self._format_url()
            save_file = self.path if type(self.path) is str else str(self.path.name)
            if Path(save_file).is_file() and not overwrite_cache:
                ds = xr.open_dataset(save_file, decode_times=False)
            else: 
                ds =  xr.open_dataset(self.iri_url + 'dods', decode_times=False, chunks='500MB')
                if verbose: 
                    print(ds)
                    ds_del = ds.to_netcdf(save_file, compute=False)
                    with ProgressBar():
                        d.compute(ds_del)
                else: 
                    ds.to_netcdf(save_file)
                ds = xr.open_dataset(save_file, decode_times=False)
            return ds
        else: 
            assert False, 'You May Not Connect To This DataSource - Either its restricted, or it has lower-level divisions and you should use one of those  '

    def _get_chunks(self):
        diffs = np.asarray([self.sliced_s[i+1] - self.sliced_s[i] for i in range(len(self.sliced_s)-1)])
        diffs2 = np.asarray([self.sliced_s[i+1] - self.sliced_s[i] for i in range(len(self.sliced_s)-1)])
        mean = np.nanmean(diffs)
        diffs[diffs <= mean] = 0 
        diffs[diffs > mean] = 1 
        if np.sum(diffs) == 0:  #uniformly distributed initialization dates 

            #### old chunk by 10 method 
            #chunkwidth = len(self.sliced_s) // 10 if len(self.sliced_s) > 20 else len(self.sliced_s) // 2
            #chunk_ndcs = []
            #ii=0
            #for i in range(0, len(self.sliced_s), chunkwidth):
            #    if i < len(self.sliced_s) - chunkwidth -1:
            #        ii=i
            #        chunk_ndcs.append([i, i + chunkwidth - 1])
            #chunk_ndcs.append([ii+1, -1])
           #### chunk by  total differences 
            #chunk_ndcs = []
            #ndx, ndx2, total = 0, 0, 0
            #while ndx2 < len(diffs2): 
            #    while total < 10 and ndx2 < len(diffs2): 
            #        total += diffs2[ndx2]
            #        ndx2 += 1
            #    chunk_ndcs.append([ndx, ndx2])
            #    ndx = ndx2 
            #    total = 0 
            #chunk_ndcs.append([ndx, ndx2+1]) #since diffs is 1 less in length than self.sliced_s - this gives len(sliced_s) - 1
            #ndx = ndx2 
            #total = 0 
            return [[i,i] for i in range(len(self.sliced_s))]  
                
        else:
            chunk_ndcs = []
            last_ndx, ndx = 0 , 0
            while ndx < len(diffs):
                if diffs[ndx] == 1:
                    chunk_ndcs.append([last_ndx, ndx])
                    last_ndx = ndx+1
                ndx += 1
            chunk_ndcs.append([last_ndx, -1])
        return chunk_ndcs

    def stream_download(self, verbose=False, overlap_targets=True): 
        assert self.may_connect, 'You May Not Connect To This DataSource - Either its restricted, or it has lower-level divisions and you should use one of those  '
        assert self.sliced_l is not None and self.sliced_s is not None, 'Must Use Kairoz to slice time first'
        if 'X' not in self.restrictions.keys() or 'Y' not in self.restrictions.keys(): 
            warnings.warn('Downloading Data without restricting or aggregating XYZSLMP-Ranges can be very slow or fail! Restrict X & Y first - S & L will be done automatically with Kairoz  ')
        
        save_file = self.path if type(self.path) is str else str(self.path.name)
        if Path(save_file).is_file():
            Path(save_file).unlink()
        
        chunks = self._get_chunks()
        print(chunks)
        for i in chunks:
            if Path(save_file).is_file():
                ds_to_be_extended = xr.open_dataset(save_file, decode_times=False)
            
            self._format_url(s_chunks=[self.sliced_s[i[0]], self.sliced_s[i[1]]])
            if verbose:
                print('\n')
                print(self.iri_url)
            ds =  xr.open_dataset(self.iri_url + 'dods', decode_times=False, chunks='500MB')
            if verbose: 
                print(ds)
                ds_del = ds.to_netcdf(save_file + '_temp', compute=False)
                with ProgressBar():
                    d.compute(ds_del)
                print('\n')
            else: 
                ds.to_netcdf(save_file+ '_temp')
            ds.close()
            ds = xr.open_dataset(save_file + '_temp', decode_times=False)
            if Path(save_file).is_file():
                new = xr.concat([ds_to_be_extended, ds], 'S')
                ds_to_be_extended.close()
                new.to_netcdf(save_file)
                Path(save_file + '_temp').unlink()
            else: 
                ds.to_netcdf(save_file)
                ds.close()
        s_vals = xr.DataArray(np.asarray([int(i) for i in self.sliced_s]), dims=['S'])
        ds = xr.open_dataset(save_file, decode_times=False)

        return ds.sel(S=s_vals)

    def rangeedges(self, grid, start, end):
        if self.coords is None: 
            if self.may_connect:
                ds = xr.open_dataset(self.url() + 'dods', decode_times=False)
                self.coords, self.units = {}, {} 
                for coord in ds.coords: 
                    self.coords[coord] = [val for val in ds.coords[coord].values]
                    self.units[coord] = ds.coords[coord].units
                self._convert_l()
                self._convert_s()
                ds.close()
            else: 
                assert False, 'cannot access -  either dataset restricted, or use a lower level variable like .prcp'

        assert grid in [coord for coord in self.coords], '{} is not a valid coordinate of this DataSource'.format(grid)
        assert start >= self.coords[grid][0], '{} out of range for {} - first value is {}'.format(start, grid, self.coords[grid][0])
        assert end <= self.coords[grid][-1], '{} out of range for {} - first value is {}'.format(start, grid, self.coords[grid][-1])

        self.restrictions[grid] = [start, end]
        return self
        
    def average(self, *args):
        if self.coords is None: 
            if self.may_connect:
                ds = xr.open_dataset(self.url() + 'dods', decode_times=False)
                self.coords, self.units = {}, {} 
                for coord in ds.coords: 
                    self.coords[coord] = [val for val in ds.coords[coord].values]
                    self.units[coord] = ds.coords[coord].units
                self._convert_l()
                self._convert_s()
                ds.close()
            else: 
                assert False, 'cannot access -  either dataset restricted, or use a lower level variable like .prcp'
        assert len(args) > 0, 'Must provide grid to average'
        
        for grid in args:
            assert grid in [coord for coord in self.coords], '{} is not a valid coordinate of this DataSource'.format(grid)
        if 'average' not in self.aggregations.keys():
            self.aggregations['average'] = []
        self.aggregations['average'].append(grid) 
        return self

    def sum(self, grid):
        if self.coords is None: 
            if self.may_connect:
                ds = xr.open_dataset(self.url() + 'dods', decode_times=False)
                self.coords, self.units = {}, {} 
                for coord in ds.coords: 
                    self.coords[coord] = [val for val in ds.coords[coord].values]
                    self.units[coord] = ds.coords[coord].units
                self._convert_l()
                self._convert_s()
                ds.close()
            else: 
                assert False, 'cannot access - either dataset restricted, or use a lower level variable like .prcp'
        assert grid in [coord for coord in self.coords], '{} is not a valid coordinate of this DataSource'.format(grid)
        if 'sum' not in self.aggregations.keys():
            self.aggregations['sum'] = []
        self.aggregations['sum'].append(grid) 
        return self
      
    def url(self):
        if self.isroot: 
            return 'http://iridl.ldeo.columbia.edu/SOURCES/'
        else: 
            if type(self.path) is str:
                t_name = [x for x in self.path.split('/') if x != ''][-1]
                t_name = t_name + '/' if t_name != '/' else t_name
                t_name = '.' + t_name if t_name[0] != '.' else t_name
            else:
                t_name = str(self.path.name) + '/' if str(self.path.name)[-1] != '/' else str(self.path.name)
                t_name = '.' + t_name if t_name[0] != '.' else t_name
            return self.parent.url() +  t_name 
        
    def _find_subsource(self):
        if '.VCAPmerged' in self.iri_url: 
            return
        subdirs = [dirname for dirname in self.path.glob('*') if dirname.is_dir()]
        files = [ str(filename.name) for filename in self.path.glob('*') if filename.is_file()]
        need_to_continue = True
        if 'index.tex' in files and not self.isroot and len(subdirs) == 0: 
            need_to_continue = False
            try:
                ds = xr.open_dataset(self.url() + 'dods', decode_times=False)
                self.coords, self.units = {} , {}
                for coord in ds.coords: 
                    self.coords[coord] = [val for val in ds.coords[coord].values]
                    self.units[coord] = ds.coords[coord].units
                self._convert_l()
                self._convert_s()
                var_names = [var for var in ds.data_vars]
                for var_name in var_names:
                    nodes = var_name.split('.')
                    cur = self 
                    for i in range(len(nodes)): 
                        clean = re.sub("([^.a-zA-Z0-9])", "_", str(nodes[i]))
                        if clean not in vars(cur).keys(): 
                            setattr(cur, clean, DataSource(nodes[i], parent=cur, find=False))
                        cur = getattr(cur, clean)
                        if i == len(nodes) - 1: 
                            setattr(cur, 'may_connect', True) 
                        else: 
                            setattr(cur, 'may_connect', False)
                        
            except: 
                need_to_continue = True
                self.may_connect = False

        if need_to_continue:
            for subdir in subdirs:
                new_child_name = re.sub("([^.a-zA-Z0-9])", "_", str(subdir.name))
                setattr(self, new_child_name, DataSource(subdir, parent=self))

    def print(self, level = 0): 
        if type(self.path) == str:
            print('  '* level + self.path)
        else:
            print('  ' * level + str(self.path.name))
        for child in vars(self).keys(): 
            if child in self.banned: 
                pass 
            elif type(getattr(self, child)) == DataSource: 
                getattr(self, child).print(level=level+1)
            else: 
                print('  '*(level+1) + child)

    def _convert_s(self):
        try:
            units, ref = handle_init_time(self.units['S'])
        
            if units == 'julian_day':
                init_times = [Day(from_julian(s)) for s in self.coords['S'].values]
            elif units == 'months':
                init_times = [Day(from_months_since(ref, s)) for s in self.coords['S']]
            elif units == 'days': 
                init_times = [Day(from_days_since(ref, s)) for s in self.coords['S']]
            elif units == 'years':
                init_times = [Day(from_years_since(ref, s)) for s in self.coords['S']]
            else:
                assert False, 'Unrecognized time units'
            
            self.coords['S_Converted'] = init_times
            self.units['S_Converted'] = units

        except:
            pass

    def _convert_l(self):
        try:
            units = self.units['L']

            assert units in ['years', 'days', 'months'], 'Unsupported lead time units'
            if units == 'months':
                lead_times = [ dt.timedelta(days=int(31.75 * s)) for s in self.coords['L']]
            elif units == 'days': 
                lead_times = [ dt.timedelta(days=int(s)) for s in self.coords['L']]
            elif units == 'years':
                lead_times = [ dt.timedelta(days=int(365.25 * s)) for s in self.coords['L']]
            else:
                assert False, 'Unrecognized lead time units'
            
            self.coords['L_Converted'] = lead_times 
            self.units['L_Converted'] = units
            
        except: 
            pass

    def kairoz(self, lead_time, target_length, training_period, overlap_targets=False, out_of_season_init=False, target_within_season=True, init_days_of_week=-1, units='days', verbose=False):
        assert type(init_days_of_week) == list or init_days_of_week == -1, 'init days of week must be a list of 3-Letter abbreviations representing days of the week for allowed  init dates'
        assert units in ['days', 'months'], 'lead time and target length must be specified in days or months'
        assert 'L_Converted' in self.units.keys(), 'L must be convertible to Day-Type'
        
        valid_init_days_of_week = []
        if init_days_of_week != -1: 
            for day in init_days_of_week: 
                is_valid = False
                for day_name in days_of_the_week.keys(): 
                    print(day.lower(), day_name)
                    if day.lower() in day_name and len(day.lower()) > 1: 
                        is_valid=True
                assert is_valid, 'Invalid day of the week - use some of "mon", "tue", "wed", "thu", "fri", "sat", "sun"' 

            for day in init_days_of_week: 
                for day_name in days_of_the_week.keys(): 
                    if day.lower() in day_name: 
                        valid_init_days_of_week.append(days_of_the_week[day_name])
        

        lead_times = self.coords['L_Converted']

        assert 'S_Converted' in self.units.keys(), 'S must be convertible to Day-Type'
        init_times = self.coords['S_Converted']

        if verbose: 
            print(lead_time, target_length, training_period.season_start, training_period.season_end)
        lead_time = convert_time_units(lead_time, units, training_period.season_start)
        tp_s = Day(training_period.season_start.datetime + dt.timedelta(days=lead_time))
        target_length = convert_time_units(target_length, units, tp_s) - 1
        tp_e = Day(tp_s.datetime + dt.timedelta(days=target_length))
        if verbose:
            print(lead_time, target_length, training_period.season_start, tp_s, tp_e, training_period.season_end)



        hi_ndx, lo_ndx = -1, 0
        for li in range(len(lead_times)-1, -1, -1):
            if lead_times[li].days > lead_time + target_length: 
                hi_ndx = li
            else:
                if  lead_times[li].days < lead_time + target_length and lead_times[li].days >= lead_time: 
                    lo_ndx = li
        #hi_ndx -= 1 # to get the highest lead time that lands within the season

        if verbose:
            print(lo_ndx, hi_ndx, lead_times[lo_ndx], lead_times[hi_ndx])
        
        s_indices = []
        previous = None
        for s in range(len(init_times)): 

            season_bound_s = Day(init_times[s].year, training_period.season_start.month, training_period.season_start.day) 
            season_bound_end_year = init_times[s].year + 1 if training_period.cross_year else init_times[s].year
            season_bound_e = Day(season_bound_end_year, training_period.season_end.month, training_period.season_end.day )
            
            comp_tp_start = Day(init_times[s].datetime + lead_times[lo_ndx]) 
            comp_tp_end = Day(init_times[s].datetime + lead_times[hi_ndx])

            is_in_training_period = (Day(init_times[s].datetime).year >= training_period.first_year and Day(init_times[s].datetime).year <= training_period.last_year)
            target_is_in_season = (season_bound_s <= comp_tp_start and  comp_tp_end <=  season_bound_e)
            target_starts_in_season = (season_bound_s <= comp_tp_start <= season_bound_e) 
            init_is_in_season = (season_bound_s <= Day(init_times[s].datetime) <= season_bound_e )  or out_of_season_init
            is_in_training_period = is_in_training_period if target_within_season else target_starts_in_season

            good_day_of_week = True if len(valid_init_days_of_week) == 0 else init_times[s].datetime.weekday() in valid_init_days_of_week

            if  is_in_training_period and target_is_in_season and init_is_in_season and good_day_of_week:
                if not overlap_targets:
                    if previous is None: 
                        s_indices.append(s)
                        if verbose:
                            print('season: ', season_bound_s,' - ',  season_bound_e, 'initialization: ',Day(init_times[s].datetime), day_codes[init_times[s].datetime.weekday()],  'target_period: ',  comp_tp_start,  comp_tp_end, 'included: ', season_bound_s <= comp_tp_start and  comp_tp_end <=  season_bound_e)
                        previous = comp_tp_end
                    else: 
                        #print(comp_tp_start, previous, comp_tp_start > previous )
                        if comp_tp_start > previous: 
                            s_indices.append(s)
                            if verbose:
                                print('season: ', season_bound_s,' - ',  season_bound_e, 'initialization: ',Day(init_times[s].datetime), day_codes[init_times[s].datetime.weekday()],  'target_period: ',  comp_tp_start,  comp_tp_end, 'included: ', season_bound_s <= comp_tp_start and  comp_tp_end <=  season_bound_e)
                            previous  = comp_tp_end
                else: 
                    if verbose:
                        print('season: ', season_bound_s,' - ',  season_bound_e, 'initialization: ',Day(init_times[s].datetime), day_codes[init_times[s].datetime.weekday()],  'target_period: ',  comp_tp_start,  comp_tp_end, 'included: ', season_bound_s <= comp_tp_start and  comp_tp_end <=  season_bound_e)
                    s_indices.append(s)

        self.sliced_s = [self.coords['S'][i] for i in s_indices]
        self.sliced_l = self.coords['L'][lo_ndx:hi_ndx]
        return self
