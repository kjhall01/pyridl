import json
from .datasource import DataSource
from .sourcetree import datalibrary1
from .utilities import * 
from .dev_utils import *
from .day import Day 
from .training_period import TrainingPeriod

dl = DataSource('.', find=False).from_dict(datalibrary1)

__version__ = "0.0.1"
__author__ = "Kyle Hall (kjhall@iri.columbia.edu)"
__license__ = "None Don't use this "
