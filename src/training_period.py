from .day import Day 

class TrainingPeriod: 
    def __init__(self,  season_start=Day(1960, 1, 1), season_end= Day(1961, 12, 31)): 
        assert type(season_start) == Day, 'invalid season start definition'
        assert type(season_end) == Day, 'invalid season end definitieion'
        self.season_start = season_start
        self.season_end = season_end
        self.cross_year = True if self.season_start.month > self.season_end.month else False
        self.first_year = self.season_start.year
        self.last_year = self.season_end.year

    def __str__(self):
        return '{:0>2}{:0>2}-{:0>2}{:0>2}_{:0>4}-{:0>4}'.format(self.season_start.month, self.season_start.day, self.season_end.month, self.season_end.day, self.first_year, self.last_year)

    def __repr__(self):
        return '{:0>2}{:0>2}-{:0>2}{:0>2}_{:0>4}-{:0>4}'.format(self.season_start.month, self.season_start.day, self.season_end.month, self.season_end.day, self.first_year, self.last_year)

    def __format__(self, fmt):
        return '{:0>2}{:0>2}-{:0>2}{:0>2}_{:0>4}-{:0>4}'.format(self.season_start.month, self.season_start.day, self.season_end.month, self.season_end.day, self.first_year, self.last_year)

