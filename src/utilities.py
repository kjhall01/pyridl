import json , os, sys , re
import numpy as np 
import datetime as dt
import xarray as xr 
from pathlib import Path 
#import xgrads as xg 
from .day import Day

days_of_the_week = {'monday': 0, 'tuesday': 1, 'wednesday': 2, 'thursday': 3, 'friday': 4, 'saturday':5, 'sunday':6 }
day_codes = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', "SUN"]
def convert_time_units(length, units, day):
    assert units in ['months', 'days'], 'unrecognized length units'
    if units == 'days':
        return length 
    else: 
        yr = day.year if day.month + length <= 12 else day.year + 1
        month = day.month + length if day.month + length <= 12 else (day.month + length ) % 12  
        return (Day(yr, month, day.day).datetime - day.datetime).days 



def rmrf(dirn):
    subfiles = [file for file in dirn.glob('*') if file.is_file()]
    subdirs = [diro for diro in dirn.glob('*') if diro.is_dir()]

    for file in subfiles: 
        file.unlink() 
    for subdir in subdirs: 
        try: 
            subdir.rmdir() 
        except: 
            rmrf(subdir)
    dirn.rmdir() 

def ls_files_recursive(dirn):
    subfiles = [file for file in dirn.glob('*') if file.is_file()]
    subdirs = [diro for diro in dirn.glob('*') if diro.is_dir()]
    files = [ file.absolute() for file in subfiles]
    for subdir in subdirs: 
        files.extend(ls_files_recursive(subdir))
    return files


def ds_cptv10(ds, cats=[], fields=[], opfile='cptv10.tsv'):
    writing_cats = False
    with open(opfile, 'w') as f: 
        f.write('xmlns:cpt=http://iri.columbia.edu/CPT/v10/' + os.linesep)
        if len(cats) > 0:  #either this 
            f.write('cpt:ncats={}'.format(len(cats)) + os.linesep)
            writing_cats = True
        elif len(fields) > 0:  #or this is true
            f.write('cpt:nfields={}'.format(len(fields)) + os.linesep)
        else: 
            assert False, 'Provide a list of fields / categories that you want to write'
        final_level = cats if writing_cats else fields
        for i in range(len(ds.coords['S'])):
            for j in range(len(ds.coords['M'])):
                for k in range(len(ds.coords['L'])):
                    for l in range(len(ds.coords['Z'])):
                        for m in range(len(final_level)):
                            field = final_level[m]
                            catvalue = m + 1
                            temp = 'cpt:C={}, '.format(catvalue) if writing_cats else ''
                            try:
                                units = ds.units[0]
                            except: 
                                units = 'mm/day'
                            try: 
                                missing = ds.missing[0]
                            except: 
                                missing = -999.0
                            line = 'cpt:field={}, {}cpt:L={}, cpt:S={}, cpt:T={},  cpt:M={},  cpt:Z={}, '.format(field, temp,  ds.coords['L'].values[k], ds.coords['S'].values[i], str(ds.coords['S'].values[i]).split('T')[0], ds.coords['M'].values[j],  ds.coords['Z'].values[l]) +  'cpt:nrow={}, cpt:ncol={}, cpt:row=Y, cpt:col=X, cpt:units={}, cpt:missing={}, '.format(len(ds.coords['Y']), len(ds.coords['X']), units, missing) + os.linesep
                            f.write(line)
                            vals = [float(val) for val in list(ds.coords['X'].values)]
                            #f.write('\t' + '\t'.join(vals) + os.linesep)
                            np.savetxt(f, np.asarray(vals), fmt="%.6f",newline='\t') 
                            f.write(os.linesep)
                            for n in range(len(ds.coords['Y'])): 
                                line = [xi  for xi in list(getattr(ds, final_level[m]).isel(S=i, M=j, L=k, Z=l, Y=n).values)]
                                line.insert(0,ds.coords['Y'].values[n]) 
                                np.savetxt(f, np.asarray(line), fmt="%.6f",newline='\t') 
                                f.write(os.linesep)


        return opfile
                        

def ncdf_ds(filename, s='S', l='L', t='T', m='M', x='X', y='Y', z='Z', fields='all'):
    assert Path(filename).absolute().is_file(), 'Cannot find {}'.format(Path(filename).absolute())
    ds = xr.open_dataset(filename, decode_times=False)
    original_coords = [coord for coord in ds.coords]
    new_coords = [s, l, t, m, x, y, z]
    good = ['S', 'L', 'T', 'M', 'X', 'Y', 'Z']
    tobechanged = {}
    for i in range(len(new_coords)):
        if new_coords[i] != good[i]:
            tobechanged[new_coords[i]] = good[i]
        if new_coords[i] not in original_coords:
            ds = ds.expand_dims(new_coords[i]).assign_coords({new_coords[i]:[1]})
    ds = ds.rename_dims(tobechanged)
    if 'T' in ds.coords and len(ds.coords['T']) > len(ds.coords['S'])* len(ds.coords['L']):
        ds = ds.rename_dims({'T':'S', 'S':'T'}) #I only anticipate this happening if there is a T dim but no S dim- generally, if there is a T dim and an S dim and an L-dim, S&L are well-defined to be == T in length
        ds = ds.mean('T')
    ds = ds.transpose('S', 'M','L', 'Z', 'Y', 'X')
    return ds


def pyridl_ds(ds, s='S', l='L', t='T', m='M', x='X', y='Y', z='Z', fields='all'):
    original_coords = [coord for coord in ds.coords]    
    new_coords = [s, l, m, x, y, z]
    good = ['S', 'L',  'M', 'X', 'Y', 'Z']
    tobechanged = {}
    for i in range(len(new_coords)):
        if new_coords[i] != good[i]:
            tobechanged[new_coords[i]] = good[i]
        if new_coords[i] not in original_coords:
            ds = ds.expand_dims(new_coords[i]).assign_coords({new_coords[i]:[1]})
    ds = ds.rename_dims(tobechanged)

    if 'T' in ds.coords and len(ds.coords['T']) > len(ds.coords['S'])* len(ds.coords['L']):
        ds = ds.rename_dims({'T':'S', 'S':'T'}) #I only anticipate this happening if there is a T dim but no S dim- generally, if there is a T dim and an S dim and an L-dim, S&L are well-defined to be == T in length
        ds = ds.mean('T')
    ds = ds.transpose('S', 'M','L', 'Z', 'Y', 'X')
    return ds
"""
def grads_ds( filename, datafilename=None): 
    assert Path(filename).absolute().is_file(), 'Cannot find {}'.format(Path(filename).absolute())
    with open(str(Path(filename).absolute()), 'r') as f: 
        content = f.read() 
        content = content.split(os.linesep)
        for line in range(len(content)): 
            content[line] = content[line].strip()
        for line in range(len(content)): 
            if 'DSET' in content[line]: 
                line2 = content[line].split() 
                line2[1] = str(Path(datafilename).absolute()) if datafilename is not None else str(Path(filename).absolute().parents[0] / Path(filename).stem) + '.dat'
                content[line] = ' '.join(line2)
        content = str(os.linesep).join(content)
    with open(str(Path(filename).absolute()), 'w') as f: 
        f.write(content)
    return xg.open_CtlDataset(str(Path(filename).absolute()))"""

def cptv10_ds(filename):
    assert Path(filename).absolute().is_file(), 'Cannot find {}'.format(Path(filename).absolute())
    with open(str(Path(filename).absolute()), 'r') as f: 
        content = f.read() 
    content = [line.strip() for line in content.split(os.linesep) ] 
    last_line_tag, row_labels_closed = 0, False
    cpt_attrs = {
        'fields': [], 
        'data': [],
        'X': [], 
        'Y': [],
        'Z': [],
        'L': [],
        'M': [],
        'S': [], 
        'T': [],
        'cats': [],
        'row_labels': [], 
        'col_labels': []
    }
    temp_array = []
    for i in range(len(content)): 
        if 'xmlns' in content[i]: 
            pass
        elif 'cpt:' in content[i]: 
            if len(temp_array) > 0: 
                cpt_attrs['data'].append(temp_array)
                temp_array = []
                row_labels_closed = True
            last_line_tag = 1
            line = [ x.replace(', ', '') for x in re.split('(cpt:?)|(cf:?)', content[i]) if x not in ['', None, 'cpt:', 'cf:']] 
            for item in line:
                if '=' in item: 
                    tag, val = item.split('=')
                    if tag  == 'field':
                        if val not in cpt_attrs['fields']:
                            cpt_attrs['fields'].append(val)
                    elif tag == 'C':
                        if val not in cpt_attrs['cats']:
                            cpt_attrs['cats'].append(val)
                    elif tag in cpt_attrs.keys(): 
                        if val not in cpt_attrs[tag]:
                            cpt_attrs[tag].append(val)
                    else: 
                        cpt_attrs[tag] = [val]
                else: 
                    item = item.split() 
                    tag, val = item[0], item[1:]
                    if tag+'_all' in cpt_attrs.keys():
                        if val not in cpt_attrs[tag+'_all']:
                            cpt_attrs[tag + '_all'].append(val)
                    else: 
                        cpt_attrs[tag+'_all'] = val
        elif last_line_tag == 1: 
            cpt_attrs['col_labels'] = [float(j) for j in content[i].split()]
            last_line_tag = 0
        else: 
            line = content[i].split()
            if len(line) > 0:
                if not row_labels_closed:
                    cpt_attrs['row_labels'].append(float(line[0]))
                temp_array.append([float(j) for j in line[1:]])
    cpt_attrs['data'].append(temp_array)

    for key in cpt_attrs.keys():
        if '_all' in key and len(cpt_attrs[key]) > 0: 
            cpt_attrs[key[:-4]] = cpt_attrs[key]
        

    if len(cpt_attrs['row_labels']) > 0: 
        row_coord = cpt_attrs['Y'] if len(cpt_attrs['row']) < 1 else cpt_attrs['row'][0] 
        cpt_attrs[row_coord] = cpt_attrs['row_labels']
    else: 
        assert False, 'No Row Labels detected'
    
    if len(cpt_attrs['col_labels']) > 0: 
        col_coord = cpt_attrs['X'] if len(cpt_attrs['col']) < 1 else cpt_attrs['col'][0] 
        cpt_attrs[col_coord] = cpt_attrs['col_labels']
    else: 
        assert False, 'No Row Labels detected'

    for key in ['S', 'X', 'Y','Z', 'L', 'M', 'S', 'cats', 'fields']:
        if len(cpt_attrs[key]) ==0: 
            cpt_attrs[key].append(0)

    data = np.asarray(cpt_attrs['data'])
    sdim, ldim = 1 if len(cpt_attrs['S']) == 0 else len(cpt_attrs['S']), 1 if len(cpt_attrs['L']) == 0 else len(cpt_attrs['L'])
    mdim, zdim = 1 if len(cpt_attrs['M']) == 0 else len(cpt_attrs['M']), 1 if len(cpt_attrs['Z']) == 0 else len(cpt_attrs['Z'])
    xdim, ydim = 1 if len(cpt_attrs['X']) == 0 else len(cpt_attrs['X']), 1 if len(cpt_attrs['Y']) == 0 else len(cpt_attrs['Y'])
    fdim, cdim = 1 if len(cpt_attrs['fields']) == 0 else len(cpt_attrs['fields']), 1 if len(cpt_attrs['cats']) == 0 else len(cpt_attrs['cats'])
    tdim = len(cpt_attrs['T'])
    if tdim > sdim * ldim:
        sdim = int(tdim / ldim  )
        cpt_attrs['S'] = [ cpt_attrs['T'][j] for j in range(0, tdim, ldim) ] 

    data = data.reshape(sdim, mdim, ldim, fdim, cdim, zdim, ydim, xdim)
    data_vars = {}
    for key in range(len(cpt_attrs['fields'])):
        for key2 in range(len(cpt_attrs['cats'])):
            data_vars[str(cpt_attrs['fields'][key]) + '_'+ str(cpt_attrs['cats'][key2])] = (['S', 'M', 'L', 'Z', 'Y', 'X'], data[:,:,:,key,key2,:,:,:])
    coords = {
        'X': cpt_attrs['X'], 
        'Y': cpt_attrs['Y'], 
        'Z': cpt_attrs['Z'], 
        'L': cpt_attrs['L'], 
        'M': cpt_attrs['M'], 
        'S': cpt_attrs['S'], 
    } 

    cpt_attrs_short = {} 
    for key in cpt_attrs: 
        if key not in ['S', 'X', 'Y','Z', 'L', 'M',  'data']:
            cpt_attrs_short[key] = cpt_attrs[key]
    return xr.Dataset( data_vars=data_vars, coords=coords, attrs=cpt_attrs_short) 




def from_ensotime(date):
    return date // 12 + 1960, date % 12 + 1, 1 

def to_ensotime(date):
    return (date.year - 1960 ) * 12 + date.month

def to_julian(dtime):
    return dtime.toordinal() + 1721424.5

def from_julian(jday):
    return Day(dt.date.fromordinal(jday - 1721424.5))

def from_months_since(ref, months):
    return Day(int(months // 12 + ref.year), int(ref.month + months % 12) , int(ref.day ))

def to_months_since(ref, date):
    return (date.year - ref.year ) * 12 + (date.month - ref.month) 

def from_days_since(ref, days):
    return Day(ref + dt.timedelta(int(days)))

def to_days_since(ref, date): 
    return (date - ref).days

def from_years_since(ref, years):
    return Day(dt.date(ref.year + int(years), ref.month, ref.day))

def to_years_since(ref, date):
    return date.year - ref.year

def handle_init_time(units): 
    if units == 'julian_day':
        return units, 0
    else: 
        tokens = [x for x in units.split(' since') if x not in ['', None]]
        assert len(tokens) >= 2, 'Invalid Time units: {}'.format(units)
        unit = tokens[0]
        reference = tokens[1]
        date_tokens = [token for token in reference.split(' ') if token not in [None, '']]
        if len(date_tokens) > 1: 
            day = dt.datetime.strptime(date_tokens[0] + ' ' + date_tokens[1], '%Y-%m-%d %H:%M:%S')
        else: 
            day = dt.datetime.strptime(date_tokens[0], '%Y-%m-%d')

        return unit, day           