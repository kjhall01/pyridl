from .datasource import DataSource
from pathlib import Path
import json




def sourcetree_from_dlentries(dlentries): 
    dlentries = Path(dlentries)
    x = DataSource(dlentries)
    x.save('temp.dl')
    with open('temp.dl', 'r') as f: 
        json_text = f.read() 
    json_text = json_text.replace('false', 'False').replace('true', 'True').replace('dlentries/entries', 'SOURCES')
    datalibrary = 'datalibrary = ' + json_text
    with open('sourcetree.py', 'w') as f: 
        f.write(datalibrary)
    return True


def load(fname):
    with open(fname, 'r') as f: 
        ret = json.load(f)
    return DataSource('.').from_dict(ret)