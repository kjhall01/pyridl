import datetime as dt 


months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
date_types = [dt.date, dt.datetime]


def check_isoformat(date):
    """returns true if date is a valid ISO-formated time string"""
    try:
        x = dt.datetime.fromisoformat(date)
        return True if x is  not None else False
    except:
        return False

def check_date_tuple(date):
    """returns true if date is of format (YYYY, MM, DD)"""
    try:
        x = dt.date(year=date[0], month=date[1], day=date[2])
    except:
        try:
            x = dt.date(year=date[0], month=months.index(date[1])+1, day=date[2])
        except:
            return False 
    return True if x is not None else False


class Day: 
    def __init__(self, year=1982, month=1, day=1):
        is_date = type(year) in date_types
        is_iso = check_isoformat(year) 
        is_tuple = check_date_tuple(year)

        assert is_date or is_iso or is_tuple or type(year) in [float, int, Day], "Date must be  date type, ISO-Formated String, or tuple with (YYYY, MM, DD) - {}".format((year, month, day))
    
        if type(year) in date_types:
            self.year, self.month, self.day = year.year, year.month, year.day
        elif type(year) is str: 
            temp = dt.datetime.fromisoformat(year)
            self.year, self.month, self.day = temp.year, temp.month, temp.day
        elif type(year) is Day:
            self.year, self.month, self.day = year.year, year.month, year.day
            self.datetime = year.datetime
        elif type(year) is tuple: 
            temp = Day(year[0], year[1], year[2])
            self.year, self.month, self.day = temp.year, temp.month, temp.day, 
        elif type(year) in [float, int]:
            try:
                temp = dt.datetime(year=int(year), month=int(month), day=int(day))
                self.year, self.month, self.day = temp.year, temp.month, temp.day
            except:
                temp = dt.datetime(year=year, month=months.index(month)+1, day=day)
                self.year, self.month, self.day = temp.year, temp.month, temp.day
        
        self.datetime = dt.datetime(year=self.year, month=self.month, day=self.day)
        self.cpt_date = '{:0>2}{}{:0>4}'.format(self.day, months[self.month-1], self.year)


    def __str__(self):
        return self.datetime.isoformat().split('T')[0]

    def __format__(self, fmt):
        return '{:0>4}{:0>2}{:0>2}'.format(self.year, self.month, self.day)

    def __repr__(self):
        return '{:0>4}{:0>2}{:0>2}'.format(self.year, self.month, self.day)

    def __lt__(self, otr):
        assert type(otr) in [Day, dt.datetime, dt.date], 'other must be day type convertible'
        if type(otr) == Day:
            return self.datetime < otr.datetime
        else:
            return self.datetime < Day(otr).datetime

    def __gt__(self, otr):
        assert type(otr) in [Day, dt.datetime, dt.date], 'other must be day type convertible'
        if type(otr) == Day:
            return self.datetime > otr.datetime
        else:
            return self.datetime > Day(otr).datetime

    def __ge__(self, otr):
        assert type(otr) in [Day, dt.datetime, dt.date], 'other must be day type convertible'
        if type(otr) == Day:
            return self.datetime >= otr.datetime
        else:
            return self.datetime >= Day(otr).datetime

    def __le__(self, otr):
        assert type(otr) in [Day, dt.datetime, dt.date], 'other must be day type convertible'
        if type(otr) == Day:
            return self.datetime <= otr.datetime
        else:
            return self.datetime <= Day(otr).datetime
    
    def __eq__(self, otr):
        assert type(otr) in [Day, dt.datetime, dt.date], 'other must be day type convertible'
        if type(otr) == Day:
            return self.datetime == otr.datetime
        else:
            return self.datetime == Day(otr).datetime



